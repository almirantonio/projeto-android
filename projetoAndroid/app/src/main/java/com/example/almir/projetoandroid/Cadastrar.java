package com.example.almir.projetoandroid;

import android.app.AlertDialog;
import android.content.Context;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;


public class Cadastrar extends AppCompatActivity {
    String codigo;
    // variavel que armazena o valor do Spinner
    Spinner listDesc;
    private  AppDatabase  appDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar);
        appDatabase = new  AppDatabase(getBaseContext());
        final Context context = this;

        findViewById(R.id.button4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                if (!IntentUtils.isAvailable(context, intent)) {
                    Toast.makeText(context, "Instale o app Barcode Scanner", Toast.LENGTH_SHORT).show();

                    if (IntentUtils.isAvailable(context, intent)) {
                        intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=com.google.zxing.client.android"));
                        startActivity(intent);
                    } else {
                        Toast.makeText(context, "Google Play nao disponivel", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    startActivityForResult(intent, 0);
                }
            }
        });
        List<Integer> op = new ArrayList<>();

        op.add(0);
        op.add(10);
        op.add(30);
        op.add(50);
        listDesc = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_dropdown_item, op);
        listDesc.setAdapter(adapter);




    }
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                 codigo = intent.getStringExtra("SCAN_RESULT");
                // Este é o texto do código de barras

                EditText t = (EditText) findViewById(R.id.editText);
                t.setText(codigo);
            }

        }

    }


    public void cadastrar(View view) {
        EditText c = (EditText) findViewById(R.id.editText);
        codigo = String.valueOf(c.getText());
        EditText nome = (EditText) findViewById(R.id.editText4);
        Editable nomeText = nome.getText();

        EditText qt = (EditText) findViewById(R.id.editText2);
        int quantidade = Integer.parseInt(String.valueOf(qt.getText()));

        EditText vu = (EditText) findViewById(R.id.editText3);
        int ValorU = Integer.parseInt(String.valueOf(vu.getText()));

        int numeroDesc = (int) listDesc.getSelectedItem();

        final Produto p = new Produto();
        p.setCodigoBarra(codigo);
        p.setNome(String.valueOf(nomeText));
        p.setQuantidade(quantidade);
        p.setValorUnit(ValorU);
        p.setDesconto(numeroDesc);
        new  Thread() {
            @Override
            public void run() {

                appDatabase.inserir(p);
            }
        }.start();





        final Intent it = new Intent(this, Listar.class);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Atenção");
       builder.setMessage("Cadastro Realizado Sucesso");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              //  startActivity(it);
                finish();
               return;
           }
        });

        AlertDialog dialog = builder.create();
       dialog.show();
    }
    }
