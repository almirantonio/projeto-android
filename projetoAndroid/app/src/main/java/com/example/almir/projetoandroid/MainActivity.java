package com.example.almir.projetoandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Cadastrar(View view) {
        Intent it = new Intent(this, Cadastrar.class);

        startActivity(it);
    }

    public void Venda(View view) {
    }

    public void Lista(View view) {
        Intent it = new Intent(this,   Listar.class);

        startActivity(it);

    }

    public void venda(View view) {
        Intent it = new Intent(this,  Venda.class);

        startActivity(it);
    }
}
