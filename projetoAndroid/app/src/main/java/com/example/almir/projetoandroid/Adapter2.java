package com.example.almir.projetoandroid;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Adapter2 extends BaseAdapter {

   private List<Produto> produto;



    private Context context;


    public Adapter2(Context context,List<Produto> lista) {
        super();
        this.context = context;
this.produto=lista;

    }




    @Override
    public int getCount() {
        return produto.size();
    }

    @Override
    public Object getItem(int position) {
        return produto.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String produtoN =  produto.get(position).getNome();
        int produtoQ =  produto.get(position).getQuantidade();
        String codigo =  produto.get(position).getCodigoBarra();
        View view = LayoutInflater.from(context).inflate(R.layout.activity_adapter2, parent, false);

        TextView t = (TextView) view.findViewById(R.id.textView21);
        t.setText("nome:"+produtoN);
        TextView h = (TextView) view.findViewById(R.id.textView20);
        h.setText("Quantidade: "+ produtoQ);
        TextView c = (TextView) view.findViewById(R.id.textView3);
        c.setText("Código: "+ codigo);

        return view;
    }


}
