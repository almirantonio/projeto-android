package com.example.almir.projetoandroid;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentManager;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

import static com.example.almir.projetoandroid.Adapter.produtoVenda;


public class Venda extends AppCompatActivity {
    String codigo;
    EditText quantidade;
    TextView valorU;
    int quantidade1;
    TextView valorTotal;
    TextView valorTotalFinal;
   public static int TotalFinal;
   public static List<Produto> produto = new ArrayList<Produto>();


    private  AppDatabase  appDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venda);
        appDatabase = new  AppDatabase(getBaseContext());
        produto=appDatabase.findAll();
        FragmentManager fm = getSupportFragmentManager();

        // Se estiver adicionado no XML
        Fragment1 frag1 = (Fragment1) fm.findFragmentById(R.id.fragment);


        frag1.listView.setAdapter(new Adapter(this));
        final Context context = this;


        findViewById(R.id.button6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                if (IntentUtils.isAvailable(context, intent)) {
                    startActivityForResult(intent, 0);
                } else {
                    Toast.makeText(context, "Instale o app Barcode Scanner", Toast.LENGTH_SHORT).show();

                    if (IntentUtils.isAvailable(context, intent)) {
                        intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=com.google.zxing.client.android"));
                        startActivity(intent);
                    } else {
                        Toast.makeText(context, "Google Play nao disponivel", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        valorTotalFinal=(TextView)findViewById(R.id.textView18);

        valorTotalFinal.setText("Valor da Comprar:"+TotalFinal);


    }
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                codigo = intent.getStringExtra("SCAN_RESULT");
                // Este é o texto do código de barras

                EditText t = (EditText) findViewById(R.id.editText5);
                t.setText(codigo);






            }

            }

        }





    public void adicionar(View view) {

        Intent t= new Intent(this, Venda.class);

        startActivity(t);

        }

    public void Calcular(View view) {
        quantidade =(EditText) findViewById(R.id.editText7);
        valorU=(TextView)findViewById(R.id.textView9);
        valorTotal=(TextView)findViewById(R.id.textView10);

        EditText e=(EditText)findViewById(R.id.editText5);
        codigo= String.valueOf(e.getText());

        for(int i=0; i<produto.size(); i++) {
            if (codigo.equals(produto.get(i).getCodigoBarra())) {

                if(produto.get(i).getQuantidade()>0) {
                valorU.setText("" + produto.get(i).getValorUnit());
                quantidade1 = Integer.parseInt(String.valueOf(quantidade.getText()));
                quantidade1 = quantidade1 * produto.get(i).getValorUnit();
                valorTotal.setText("" + quantidade1);
                produto.get(i).setValorTotal(quantidade1);

                    produtoVenda.add(produto.get(i));

                    Toast.makeText(this, "Produto: " +  produto.get(i).getNome(), Toast.LENGTH_LONG).show();
                    TotalFinal = TotalFinal + quantidade1;
                    produto.get(i).setQuantidade(produto.get(i).getQuantidade() - quantidade1);
                    appDatabase.alterar(produto.get(i));
                }else {
                    if(produto.get(i).getQuantidade()<=0){
                        appDatabase.delete(produto.get(i).getCodigo());
                    }
                    Toast.makeText(this, "Produto: " + produto.get(i).getNome()+" Não existe no Estoque", Toast.LENGTH_LONG).show();

                    Intent t= new Intent(this, Venda.class);

                    startActivity(t);
                }
                }

        }
    }

    public void finalizar(View view) {

        produtoVenda.clear();

        finish();

        Intent t= new Intent(this, MainActivity.class);

        startActivity(t);


    }
}
