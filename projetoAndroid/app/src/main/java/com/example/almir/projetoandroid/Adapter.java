package com.example.almir.projetoandroid;


import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Adapter extends BaseAdapter {

    public static List<Produto> produtoVenda = new ArrayList<Produto>();



    private Context context;


    public Adapter(Context context) {
        super();
        this.context = context;
    }




    @Override
    public int getCount() {
        return produtoVenda.size();
    }

    @Override
    public Object getItem(int position) {
        return produtoVenda.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String produtoN =  produtoVenda.get(position).getNome();
        int produtoQ =  produtoVenda.get(position).getQuantidade();
        int vt =  produtoVenda.get(position).getValorTotal();

        View view = LayoutInflater.from(context).inflate(R.layout.activity_adapter, parent, false);

        TextView nome = (TextView) view.findViewById(R.id.textView12);
        nome.setText(""+produtoN);
        TextView Vtotal = (TextView) view.findViewById(R.id.textView13);
        Vtotal.setText(""+produtoQ);
        TextView h = (TextView) view.findViewById(R.id.textView11);
        h.setText(""+vt);


        return view;
    }


}
