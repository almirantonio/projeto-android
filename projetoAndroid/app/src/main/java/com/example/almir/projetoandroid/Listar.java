package com.example.almir.projetoandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;



public class Listar extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private ListView listView;

    private AppDatabase appDatabase;
    Adapter2 adapter2;
    List<Produto> produtoLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);

        appDatabase = new  AppDatabase(getBaseContext());

        produtoLista=appDatabase.findAll();



        listView = (ListView) findViewById(R.id.lista);
        listView.setAdapter(new Adapter2(this,produtoLista));
        listView.setOnItemClickListener(this);

    }
    public void onItemClick(AdapterView<?> parent, View view, int idx, long id) {

        Intent t= new Intent(this, informacoes.class);
        informacoes.id=idx;
        startActivity(t);

    }

}
