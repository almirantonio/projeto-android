package com.example.almir.projetoandroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class AppDatabase extends SQLiteOpenHelper {
    private static final String TAG = "sql";

    // Nome do banco
    private static final String NOME_BANCO = "fafica.mobile";
    private static final int VERSAO_BANCO = 1;

    public AppDatabase(Context context) {
        super(context, NOME_BANCO, null, VERSAO_BANCO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "Criando a Tabela produto...");
        db.execSQL("CREATE TABLE IF NOT EXISTS produto " +
                "(codigo integer PRIMARY KEY AUTOINCREMENT," +
                "codigoBarra TEXT, " +
                "nome TEXT, " +
                "valorUnit INTERGER, " +
                "valorDes INTERGER, " +
                "quantidade INTERGER);");
        Log.d(TAG, "Tabela Produto criada com sucesso.");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Caso mude a versão do banco de dados, podemos executar um SQL aqui
        if (oldVersion == 1 && newVersion == 2) {
            db.execSQL("ALTER TABLE produto ADD dataAniversario DATE");
        }
    }




    // Insere um nova pessoa
    public long inserir(Produto produto) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("nome", produto.getNome());
            values.put("quantidade", produto.getQuantidade());
            values.put("codigoBarra", produto.getCodigoBarra());
            values.put("valorUnit", produto.getValorUnit());
            values.put("valorDes", produto.getDesconto());
            // insert into carro values (...)
            long id = db.insert("produto", "", values);

            Log.v(TAG, "Pessoa adicionada com sucesso" + produto.getNome());
            return id;

        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        } finally {
            db.close();
        }
    }

    // Consulta a lista com todas as pessoas
    public ArrayList<Produto> findAll() {
        SQLiteDatabase db = getReadableDatabase();
        try {
            // select * from carro
            Cursor c = db.query("produto", null, null, null, null, null, null, null);

            ArrayList<Produto> listaProduto = new ArrayList<Produto>();
            for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
                // The Cursor is now set to the right position
                Produto p = new Produto();

                p.setCodigo(c.getInt(c.getColumnIndex("codigo")));
                p.setNome(c.getString(c.getColumnIndex("nome")));
                p.setQuantidade(c.getInt(c.getColumnIndex("quantidade")));
                p.setCodigoBarra(c.getString(c.getColumnIndex("codigoBarra")));
                p.setValorUnit(c.getInt(c.getColumnIndex("valorUnit")));
                p.setDesconto(c.getInt(c.getColumnIndex("valorDes")));
                listaProduto.add(p);

            }
            return listaProduto;
        } finally {
            db.close();
        }
    }

    public void delete(long id) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.delete("produto", "codigo=" + id, null);
            //db.delete("pessoas", "codigo=?", new String[]{String.valueOf(id)});
            Log.v(TAG, "Produto deletada com sucesso");
        } finally {
            db.close();
        }

    }


    public long alterar(Produto produto) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("nome", produto.getNome());
            values.put("quantidade", produto.getQuantidade());
            values.put("quantidade", produto.getCodigoBarra());
            values.put("quantidade", produto.getValorUnit());
            values.put("quantidade", produto.getDesconto());

            long id = db.update("produto", values, "codigo = "+produto.getCodigo(),null);

            Log.v(TAG, "Produto alterada com sucesso" + produto.getNome());
            return id;

      } catch (Exception e) {
            e.printStackTrace();
           return 0;
        } finally {
            db.close();
        }
    }
}
